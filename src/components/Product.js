import React from "react";
import { Link } from 'react-router-dom';
import loadjs from 'loadjs';

class Product extends React.Component {
    componentDidMount() {
        loadjs('/script.js', function() {
        });
        this.setState((state, props) => {
          return {path: this.inputRef.current.innerHTML};
        });
    }
    
    constructor(props) {
        super(props);
        this.state = {
            path: 0
        }
        this.inputRef = React.createRef();
      }
    
    
    updatePath() {
        this.setState((state, props) => {
          return {path: this.inputRef.current.innerHTML};
        });
    }
    
    render() {
  return (
    <div id="product-wrapper">
      <div id="left-wrapper">
      <Link to='/'><button id="log-out">Log Out</button></Link>
      <div id="carousel" className="carousel slide" data-ride="carousel">
          <div className="carousel-inner">
            <div className="carousel-item active">
              <img className="d-block w-100" src="/images/tacos.jpeg" alt="First slide" />
            </div>
            <div className="carousel-item">
              <img className="d-block w-100" src="/images/tacos2.jpeg" alt="Second slide" />
            </div>
            <div className="carousel-item">
              <img className="d-block w-100" src="/images/tacos3.jpeg" alt="Third slide" />
            </div>
          </div>
          <a className="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
            <span className="sr-only">Previous</span>
          </a>
          <a className="carousel-control-next" href="#carousel" role="button" data-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true"></span>
            <span className="sr-only">Next</span>
          </a>
        </div>
      </div>
        <div id="product-section">
            <h1>Tacos Al Pastor</h1>
            <h3>£8.99 <span id="delivery">free delivery</span></h3>
            <div id="add-to-basket">
                <p id="in-stock"><svg xmlns="http://www.w3.org/2000/svg" id="in-stock-icon" viewBox="0 0 24 24" fill="none" strokeLinecap="round" strokeLinejoin="round">
                  <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                  <polyline points="9 11 12 14 20 6" />
                  <path d="M20 12v6a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2h9" />
                </svg>In stock</p>
                <button className="change-basket" id="less-basket">-</button>
                <p id="add-number">0</p>
                <button className="change-basket" id="more-basket">+</button>
                <button id="add-button" onClick={this.updatePath.bind(this)}>Add To Basket</button>
            </div>
            <h2>Description</h2>
            <p id="description">Tacos al pastor, is a taco made with spit-grilled pork. Based on the lamb shawarma brought by Lebanese immigrants to Mexico, al pastor features a flavor palate that combines traditional Middle Eastern spices with those indigenous to central Mexico. It is a popular street food that has spread to the United States.</p>
        </div>
        <div id="basket">
            <h1>Your Basket</h1>
            <div id="basket-item">
                <div id="basket-img"></div>
                <p id="basket-name">Tacos Al Pastor</p>
                <p id="basket-price">£8.99</p>
                <p id="basket-amount" ref={this.inputRef}>0</p>
                <button id="delete-basket" onClick={this.updatePath.bind(this)}>X</button>
            </div>
            <div id="subtotal"><p id="total-text">Subtotal</p><p id="total-price">£0.00</p></div>
            <Link to={'/checkout/' + this.state.path}><button id="go-to-checkout" >Checkout</button></Link>
        </div>
    </div>
  );
}
}

export default Product;























