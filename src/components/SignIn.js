import React from "react";
import loadjs from 'loadjs';

class SignIn extends React.Component {
    componentDidMount() {
        loadjs('/script.js', function() {
        });
    }
    
    render() {
        
      return (
        <div id="sign-in-wrapper">
            <h1>Sign in</h1>
            <form id="sign-in">
                <input type="text" placeholder="Username or email address" className="text-inputs" id="username" />
                <div id="username-error"><svg xmlns="http://www.w3.org/2000/svg" id="warning-icon" viewBox="0 0 24 24" fill="none" strokeLinecap="round" strokeLinejoin="round">
                  <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                  <path d="M12 9v2m0 4v.01" />
                  <path d="M5 19h14a2 2 0 0 0 1.84 -2.75l-7.1 -12.25a2 2 0 0 0 -3.5 0l-7.1 12.25a2 2 0 0 0 1.75 2.75" />
                </svg>Please ensure there is no whitespace at the start or end.</div>
                <input type="password" placeholder="Password" className="text-inputs" id="password" />
                <div id="keep-signed-in"><input type="checkbox" name="check" /><label htmlFor="check">Keep me signed in</label></div>
                <button type="submit">Login</button>
            </form>
            <p>I forgot my password</p>
            <p>Resend verification email</p>
        </div>
      );
    }
}

export default SignIn;