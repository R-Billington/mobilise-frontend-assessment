import './App.css';
import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import SignIn from './components/SignIn.js';
import Product from './components/Product.js';
import Checkout from './components/Checkout.js';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/" exact component={() => <SignIn />} />
          <Route path="/product/:amount" exact component={() => <Product />} />
          <Route path="/checkout/:amount" exact component={() => <Checkout />} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
