$(document).ready(() => {
    (function($) {
      $.fn.inputFilter = function(inputFilter) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
          if (inputFilter(this.value)) {
            this.oldValue = this.value;
            this.oldSelectionStart = this.selectionStart;
            this.oldSelectionEnd = this.selectionEnd;
          } else if (this.hasOwnProperty("oldValue")) {
            this.value = this.oldValue;
            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
          } else {
            this.value = "";
          }
        });
      };
    }(jQuery));
    
    $('#username').on('change textInput input', (e) => {
        let username = $('#username').val();
        if (/^\s/.test(username) || /\s$/.test(username)) {
            $('#username-error').fadeIn(500);
            $('#username').css('border-color', '#9c191b');
        } else {
            $('#username-error').fadeOut(500);
            $('#username').css('border-color', '#c4c4cc');
        }
    })

    $('form').submit(function (event) {
        event.preventDefault();
        let username = $('#username').val();
        let password = $('#password').val();
        if (username === "user" && password === "password") {
            window.location.href = '/product/0';
        } else {
            Swal.fire({
                text: 'Username or password incorrect',
                icon: 'error'
            })
        }
        
    });
    
    ///////////////////////////
    // PRODUCT SECTION
    //////////////////////////
    let path = window.location.pathname;
    let number = path.slice(-1);
    
    if (path.slice(-2) === '10') {
        number = '10'
    }
    $('#basket-amount').text(number);
    if (parseInt(number, 10) > 0) {
        $('#basket-item').css('visibility', 'visible');
    }
    
    
    $('.carousel').carousel();
    let totalInBasket = parseInt(number, 10);
    
     $('#more-basket').unbind('click').on('click', () => {
        let amount = parseInt($('#add-number').text(), 10);
        if (amount < 10) {
            amount += 1;
            $('#add-number').text((amount).toString());
        }
    })
    
    $('#less-basket').unbind('click').on('click', () => {
        let amount = parseInt($('#add-number').text(), 10);
        if (amount > 0) {
            amount -= 1;
            $('#add-number').text((amount).toString());
        }
    })
    
    $('#add-button').on('click', () => {
        let amount = parseInt($('#add-number').text(), 10);
        if (amount > 0 && totalInBasket + amount <= 10) {
            totalInBasket += amount;
            $('#basket-item').css('visibility', 'visible');
            $('#basket-amount').text(totalInBasket);
            let subtotal = (totalInBasket * 8.99).toFixed(2);
            $('#total-price').text('£' + subtotal);
            $('#add-number').text('0');
        } else if (amount + totalInBasket > 10) {
            Swal.fire({
                text: "Only 10 of this item is allowed in the basket.",
                icon: 'warning',
            });
        }
    })
    
    $('#delete-basket').on('click', () => {
        totalInBasket = 0;
        $('#basket-amount').text('0');
        $('#basket-item').css('visibility', 'hidden');
        $('#total-price').text('£0.00');
    })
    
    
    /////////////////////////////
    // CHECKOUT PAGE ////////////
    ////////////////////////////
    
    
    
    $('#checkout-number').text(number);
    let subtotal = (parseInt(number, 10) * 8.99).toFixed(2);
    $('#checkout-total-price').text('£' + subtotal);
    
    $('#card-name').on('change textInput input', () => {
        let cardName = $('#card-name').val();
        $('#card-image-name').text(cardName);
    });
    
    $("#card-number, #cvv").inputFilter(function(value) {
        return /^\d*$/.test(value);
    });
    
    $('#card-number').on('change textInput input', () => {
        let cardNumber = $('#card-number').val();
        let displayStr = cardNumber;
        while (displayStr.length < 16) {
            displayStr += '•';
        }
        $('#cn1').text(displayStr.substr(0, 4));
        $('#cn2').text(displayStr.substr(4, 4));
        $('#cn3').text(displayStr.substr(8, 4));
        $('#cn4').text(displayStr.substr(12, 4));
    })
    
    
    $('#month, #year').on('change', () => {
        let month = $('#month').val();
        let year = $('#year').val().substr(2, 2);
        if (month.length === 1) {
            month = '0' + month;
        }
        if (month.length && year.length) {
            $('#card-image-date').text(month + ' / ' + year);
        } else if (year.length) {
            $('#card-image-date').text('•• / ' + year);
        } else {
            $('#card-image-date').text(month + ' / ••');
        }
    })
    
    $('#more-checkout').unbind('click').on('click', () => {
        let amount = parseInt($('#checkout-number').text(), 10);
        if (amount < 10) {
            amount += 1;
            $('#checkout-number').text((amount).toString());
        }
        let subtotal = (amount * 8.99).toFixed(2);
        $('#checkout-total-price').text('£' + subtotal);
    })
    
    $('#less-checkout').unbind('click').on('click', () => {
        let amount = parseInt($('#checkout-number').text(), 10);
        if (amount > 1) {
            amount -= 1;
            $('#checkout-number').text((amount).toString());
        }
        let subtotal = (amount * 8.99).toFixed(2);
        $('#checkout-total-price').text('£' + subtotal);
    })
    
    $('#delete-item').on('click', () => {
        $('#checkout-number').text('0');
        $('#checkout-total-price').text('£0.00');
        $('#checkout-item').css('visibility', 'hidden');
    })
    
    $('#finish-checkout').on('click', () => {
        if ($('#card-name').val() != '' && $('#card-number').val().length == 16 && $('#month').val() != '' && $('#year').val() != '' && $('#cvv').val() != '') {
            Swal.fire({
              title: 'Processing Payment',
              timer: 2000,
              timerProgressBar: true,
              didOpen: () => {
                Swal.showLoading()
                timerInterval = setInterval(() => {
                  const content = Swal.getContent()
                  if (content) {
                    const b = content.querySelector('b')
                    if (b) {
                      b.textContent = Swal.getTimerLeft()
                    }
                  }
                }, 100)
              },
              willClose: () => {
                clearInterval(timerInterval)
              }
            }).then(() => {
                Swal.fire(
                  'Success!',
                  'Your payment has been processed.',
                  'success'
                )
            })
        } else {
            Swal.fire({
                text: "Please fill out all fields of the form",
                icon: 'warning',
            });
        }
    })
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
})
