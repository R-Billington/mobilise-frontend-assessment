<?php

$errors = [];
$data = [];

if (empty($_POST['username'])) {
    $errors['name'] = 'Name is required.';
}

if (empty($_POST['password'])) {
    $errors['email'] = 'Email is required.';
}

if (!empty($errors)) {
    $data['success'] = false;
    $data['errors'] = $errors;
} else {
    $data['success'] = true;
    $data['message'] = 'Success!';
}

echo json_encode($data);